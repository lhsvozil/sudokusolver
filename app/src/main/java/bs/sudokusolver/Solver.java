package bs.sudokusolver;

import android.util.Log;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by stras_000 on 5. 6. 2015.
 */
public class Solver {
    public int[][] numbers;
    ArrayList<String> textEditValues;
    private Set<Integer>[][] plausibleMatrix;
    String log;

    private int[] numbersCount;

    public Solver() {
        // Object o = (editT.get(0).getText().toString());
        log = "";
        numbers = new int[9][9];
        plausibleMatrix = new HashSet[9][9];

        for (int i = 0; i < 81; i++) {
            plausibleMatrix[(i / 9)][i % 9] = new HashSet<Integer>();
        }

        for (int i = 0; i < 81; i++) {
            numbers[(i / 9)][i % 9] = 0;
        }
    }

    public Solver(ArrayList<String> values) {
        // Object o = (editT.get(0).getText().toString());
        log = "";
        numbers = new int[9][9];
        plausibleMatrix = new HashSet[9][9];

        for (int i = 0; i < 81; i++) {
            plausibleMatrix[(i / 9)][i % 9] = new HashSet<Integer>();
        }

        for (int i = 0; i < 81; i++) {
            int ind = i / 9;
            int indY = i % 9;
            numbers[(i / 9)][i % 9] = (values.get(i).equals("")) ? 0 : Integer.valueOf(values.get(i));
        }/*
        numbers[0][1] = 8;
        numbers[0][6] = 2;
        numbers[1][4] = 8;
        numbers[1][5] = 4;
        numbers[1][7] = 9;
        numbers[2][2] = 6;
        numbers[2][3] = 3;
        numbers[2][4] = 2;
        numbers[2][7] = 1;
        numbers[3][1] = 9;
        numbers[3][2] = 7;
        numbers[3][7] = 8;
        numbers[4][0] = 8;
        numbers[4][3] = 9;
        numbers[4][5] = 3;
        numbers[4][8] = 2;
        numbers[5][1] = 1;
        numbers[5][6] = 9;
        numbers[5][7] = 5;
        numbers[6][1] = 7;
        numbers[6][4] = 4;
        numbers[6][5] = 5;
        numbers[6][6] = 8;
        numbers[7][1] = 3;
        numbers[7][3] = 7;
        numbers[7][4] = 1;
        numbers[8][2] = 8;
        numbers[8][7] = 4;
*/
        /*numbers[0][2] = 8;
        numbers[0][3] = 5;
        numbers[0][6] = 7;
        numbers[0][8] = 1;
        numbers[1][2] = 2;
        numbers[1][7] = 6;
        numbers[2][0] = 9;
        numbers[2][5] = 8;
        numbers[3][0] = 2;
        numbers[3][1] = 3;
        numbers[3][3] = 8;
        numbers[4][2] = 7;
        numbers[4][4] = 6;
        numbers[4][6] = 9;
        numbers[5][5] = 9;
        numbers[5][7] = 5;
        numbers[5][8] = 2;
        numbers[6][3] = 3;
        numbers[6][8] = 7;
        numbers[7][1] = 2;
        numbers[7][6] = 8;
        numbers[8][0] = 1;
        numbers[8][2] = 6;
        numbers[8][5] = 2;
        numbers[8][6] = 3;*/

        int nulls = 5;

     /*   while (true)
        {
            solve();
            iterateThroughNumbers();

            nulls = 0;
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (numbers[i][j] == 0)
                    {
                        nulls++;
                    }
                }
            }
             if (nulls == 0)
                 break;
        }*/
        //this.editTexts=editT;
    }

    public void mainSolvingFunc(){
        for (int i = 0; i < 150; i++) {
            solve();
            iterateThroughNumbers();
        }
    }

/*    private int countNumbersAndReturnBiggest(){
        numbersCount = new int[10];
        int result = 1;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
            numbersCount[numbers[i][j]] +=1;
            }
        }
        for (int i = 1; i < 10; i++) {
            result = (numbersCount[i] > numbersCount[result]) ? i:result;
        }

        return result;
    }*/

    public void solve() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                plausibleMatrix[i][j].clear();
                if (numbers[i][j] == 0) {
                    for (int k = 1; k < 10; k++) {
                        if (!(isInColumn(j, k) || isInRow(i, k) || isInGroup(k, i, j))) {
                            plausibleMatrix[i][j].add(k);
                        }
                    }
                    if (plausibleMatrix[i][j].size() == 1) {
                        numbers[i][j] = (int) plausibleMatrix[i][j].toArray()[0];
                        log += "Prirad - Radek: " + i + " Sloupec: " + j + " Number: " + numbers[i][j] + "\n";
                    }
                }
            }
        }
        return;
    }

    private boolean isInGroup(int number, int r, int c) {
        int offsetR = r / 3;
        int offsetC = c / 3;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (numbers[offsetR * 3 + i][offsetC * 3 + j] == number)
                    return true;
            }
        }


        return false;
    }

    private boolean isInRow(int row, int number) {

        for (int i = 0; i < 9; i++) {
            if (numbers[row][i] == number) {
                return true;
            }
        }
        return false;
    }

    private boolean isInColumn(int col, int number) {

        for (int i = 0; i < 9; i++) {
            if (numbers[i][col] == number) {
                return true;
            }
        }
        return false;
    }

    private boolean iterateThroughNumbers() {
        for (int cislo = 1; cislo < 10; cislo++) {
            int found = 0;
            int r = 0, c = 0;
            for (int i = 0; i < 9; i++) {
                if (!isInRow(i, cislo)) {
                    for (int j = 0; j < 9; j++) {
                        if (plausibleMatrix[i][j].contains(cislo)) {
                            found++;
                            r = i;
                            c = j;
                        }
                    }
                    if (found == 1) {
                        numbers[r][c] = cislo;
                        log += "Prirad - Radek: " + r + " Sloupec: " + c + " Number: " + cislo + "\n";
                    }
                    found = 0;
                }

                if (!isInColumn(i, cislo)) {
                    for (int j = 0; j < 9; j++) {
                        if (plausibleMatrix[j][i].contains(cislo)) {
                            found++;
                            r = j;
                            c = i;
                        }
                    }
                    if (found == 1) {
                        numbers[r][c] = cislo;
                        log += "Prirad - Radek: " + r + " Sloupec: " + c + " Number: " + cislo + "\n";
                    }
                    found = 0;
                }
            }/*
            for (int ig = 0; ig < 3; ig++) {
                for (int jg = 0; jg < 3; jg++) {

                    if (!isInGroup(cislo, ig*3,jg*3)) {

                        for (int i = 0; i < 3; i++) {
                            for (int j = 0; j < 3; j++) {
                                if (plausibleMatrix[ig * 3 + i][jg * 3 + j].contains(cislo)) {
                                    found++;
                                    r = i;
                                    c = j;
                                }
                            }
                        }


                        if (found == 1) {
                            numbers[r][c] = cislo;
                            log += "Prirad - Radek: " + r + " Sloupec: " + c + " Number: " + cislo + "\n";
                        }
                        found = 0;
                    }
                    }
                }*/
            }





        return false;
    }

    public ArrayList<String> getValues() {
        textEditValues = new ArrayList<String>();
        for (int i = 0; i < 81; i++) {
            textEditValues.add(i, String.valueOf(numbers[i / 9][i % 9]));
        }

        return textEditValues;
    }
}
