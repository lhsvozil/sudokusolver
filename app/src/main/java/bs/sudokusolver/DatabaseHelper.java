package bs.sudokusolver;

import android.app.DownloadManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.widget.CursorAdapter;

import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SudokuSolverData";
    public static final int DATABASE_VERSION = 1;

    public static final class Sudoku {
        public static final String TABLE_NAME = "sudokuDetail";
        public static final String COLUMN_ID = BaseColumns._ID;
        public static final String COLUMN_SUDOKUID = "sudoku_id";
        public static final String COLUMN_ROW = "row";
        public static final String COLUMN_COL = "col";
        public static final String COLUMN_VALUE = "value";
        //public static final String COLUMN_RATING = "rating";
    }
    public static final class ListOfSudokus {
        public static final String TABLE_NAME = "sudoku";
        public static final String COLUMN_ID = BaseColumns._ID;
        public static final String COLUMN_VALUESCOUNT = "valCount";
        //public static final String COLUMN_RATING = "rating";
    }


    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Sudoku.TABLE_NAME + "(" + Sudoku.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Sudoku.COLUMN_SUDOKUID + " INTEGER, " + Sudoku.COLUMN_ROW + " INTEGER, " + Sudoku.COLUMN_COL + " INTEGER, "
                + Sudoku.COLUMN_VALUE + " INTEGER)");
        db.execSQL("CREATE TABLE " + ListOfSudokus.TABLE_NAME + "(" + ListOfSudokus.COLUMN_ID + " INTEGER PRIMARY KEY, "
                + ListOfSudokus.COLUMN_VALUESCOUNT + " INTEGER)");
    }

    public void insert(SQLiteDatabase db, int numbers[][]){

        ContentValues newSudokuRow = new ContentValues();
        Cursor maxId = db.rawQuery("SELECT MAX("+Sudoku.COLUMN_SUDOKUID+") FROM " + Sudoku.TABLE_NAME, null);
        maxId.moveToFirst();
        int sudokuID = maxId.getInt(0) + 1;
        int valuesCount = 0;
        for(int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++) {
                if (numbers[i][j] == 0){
                    continue;
                }
                ContentValues newRow = new ContentValues();
                newRow.put(Sudoku.COLUMN_SUDOKUID, sudokuID);
                newRow.put(Sudoku.COLUMN_COL, j);
                newRow.put(Sudoku.COLUMN_ROW, i);
                newRow.put(Sudoku.COLUMN_VALUE, numbers[i][j]);

                valuesCount++;
                db.insert(Sudoku.TABLE_NAME, null, newRow);

            }
        }
        newSudokuRow.put(ListOfSudokus.COLUMN_ID, sudokuID);
        newSudokuRow.put(ListOfSudokus.COLUMN_VALUESCOUNT, valuesCount);
        db.insert(ListOfSudokus.TABLE_NAME, null, newSudokuRow);
    }

    public int[][] getSudoku(SQLiteDatabase db, int sudokuID){
        int[][] result = new int[9][9];
        Cursor s = db.rawQuery("SELECT "+Sudoku.COLUMN_ROW+", "+Sudoku.COLUMN_COL+", "+Sudoku.COLUMN_VALUE+" FROM "+Sudoku.TABLE_NAME+" WHERE "+Sudoku.COLUMN_SUDOKUID+" = "+sudokuID, null);
        while(s.moveToNext()){
            result[s.getInt(0)][s.getInt(1)] = s.getInt(2);
        }
        s.close();
        return result;
    }

    public Cursor getSudokus(SQLiteDatabase db){
        return db.rawQuery("SELECT "+ListOfSudokus.COLUMN_ID+", "+ListOfSudokus.COLUMN_VALUESCOUNT+" FROM " + ListOfSudokus.TABLE_NAME, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST" + Sudoku.TABLE_NAME);
        onCreate(db);
    }
}
