package bs.sudokusolver;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import bs.sudokusolver.Solver;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class TypeInActivity extends ActionBarActivity {

    //private ArrayList<EditText> editMatrix;
    private ArrayList<TextView> editMatrix;
    private ArrayList<String> stringValues;
    private Solver s;
    private GridView gridView;
    private ArrayAdapter<String> adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_in);

        createTextEdits();

        Intent in = getIntent();
        int sudokuId = in.getIntExtra("id",0);

        if (sudokuId > 0) {
            stringValues = new ArrayList<String>();
            DatabaseHelper myHelper = new DatabaseHelper(this);
            SQLiteDatabase myDb = myHelper.getReadableDatabase();
            Cursor cur = myHelper.getSudokus(myDb);
            s = new Solver();
            s.numbers = myHelper.getSudoku(myDb,sudokuId);

            stringValues = s.getValues();

            for (int i = 0; i < 81; i++){
                editMatrix.get(i).setText(stringValues.get(i));
            }
        }

    }



    public void solveButtonClick(View v) {
        // stringValuesFromEditTexts();
        int ij = 5;
        stringValues = new ArrayList<String>();

        for (int i = 0; i < 81; i++)
            stringValues.add("0");

        Solver s = new Solver(stringValues);
        //s.numbers[0][0]=5;
        s.mainSolvingFunc();

        stringValues = s.getValues();
        String[] tmp = new String[stringValues.size()];
        tmp = stringValues.toArray(tmp);

        gridView = (GridView) findViewById(R.id.gridView);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,tmp);

        gridView.setAdapter(adapter);

    /*    for (int i = 0; i < 81; i++){
            editMatrix.get(i).setText(stringValues.get(i));
        }*/


       // s.write();
    }

    public void saveButtonClick(View v){
        DatabaseHelper myHelper = new DatabaseHelper(this);
        SQLiteDatabase myDb = myHelper.getWritableDatabase();

        stringValuesFromEditTexts();
        s = new Solver(stringValues);
        myHelper.insert(myDb,s.numbers);
        // s.write();
    }

    private void stringValuesFromEditTexts(){
        stringValues = new ArrayList<String>();
        for (int i = 0; i < 81; i++){
            stringValues.add(editMatrix.get(i).getText().toString());
        }
    }

    private void createTextEdits(){
        editMatrix = new ArrayList();
       // GridLayout container = (GridLayout)findViewById(R.id.gridL);

        for (int i = 0; i < 81; i++){
            //EditText edt = new EditText(this);
            TextView edt = new TextView(this);
            edt.setWidth(90); //napsat do konstanty
            edt.setHeight(90);

            edt.setId(i + 1);
            edt.setBackgroundColor(Color.RED);
            edt.setPadding(5, 5, 5, 5);

            editMatrix.add(edt);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_type_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
